
function lerp(a, b, p)
   return (a * (1 - p)) + (b * p)
end

local images = {}

local Vect2i = {}
Vect2i.__index = Vect2i

function Vect2i:new(x, y)
   local obj = {
      x = 0,
      y = 0
   }
   setmetatable(obj, self);
   obj:set(x, y)
   return obj
end

function Vect2i:set(x, y)
   if type(x) == 'table' then
      local t = x
      x = t.x
      y = t.y
   end
   self.x = math.floor(x) or 0
   self.y = math.floor(y) or 0
end

function Vect2i:copy()
   return Vect2i:new(self.x, self.y)
end

function Vect2i:round()
   self.x = math.floor(self.x)
   self.y = math.floor(self.y)
end

function Vect2i:addScaled(vect, scale)
   self.x = self.x + vect.x * scale
   self.y = self.y + vect.y * scale
   self:round()
end

function Vect2i:lerp(a, b, p)
   local x = lerp(a.x, b.x, p)
   local y = lerp(a.y, b.y, p)
   return Vect2i:new(x, y)
end

local server = {
   updateTimer = 0,
   updateTimeout = 1,
   ship = {
      pos = Vect2i:new(30, 30),
      vel = Vect2i:new(0, 0),
      ang = 0
   }
}
local client = {
   ship = {
      previous = {
         pos = Vect2i:new(30, 30),
         vel = Vect2i:new(0, 0),
         ang = 0
      },
      current = {
         pos = Vect2i:new(30, 30),
         vel = Vect2i:new(0, 0),
         ang = 0
      }
   }
}



function integrate(pos, vel, dt)
   pos:addScaled(vel, dt / 1000) 
end

function love.load()
   images.ship = love.graphics.newImage("ship.png")
end

function server.getEstimatedPercent()
   return (1 - (server.updateTimer / server.updateTimeout))
end

function server.sendToClient()
   local client_current = client.ship.current
   local client_previous = client.ship.previous
   local server_ship = server.ship 

   -- set client current to previous
   client_previous.pos:set(client_current.pos)
   client_previous.vel:set(client_current.vel)
   client_previous.ang = client_current.ang
   
   client_current.pos:set(server_ship.pos)
   client_current.vel:set(server_ship.vel)
   client_current.ang = server_ship.ang
 
end

function server.update(dt)
   local ship = server.ship
   integrate(ship.pos, ship.vel, dt)
   --ship.ang = ship.ang + 6 * dt

   local rotationSpeed = 0.3
   local deltaVelocity = 30  

   if love.keyboard.isDown('a') then
      ship.ang = ship.ang + rotationSpeed * dt
   elseif love.keyboard.isDown('d') then
      ship.ang = ship.ang - rotationSpeed * dt
   end
   if love.keyboard.isDown('w') then
      client.addVelocity(ship.vel, ship.ang, deltaVelocity * dt)
   elseif love.keyboard.isDown('s') then
      client.addVelocity(ship.vel, ship.ang, -deltaVelocity * dt)
   end

   server.sendToClient() 

end

function client.addVelocity(vel, ang, dv)
   local ajAng = 6.28 - ang
   local dx = math.cos(ajAng) * dv * 1000
   local dy = math.sin(ajAng) * dv * 1000
   --print(dx, dy)
   vel.x = vel.x + dx
   vel.y = vel.y + dy
   print(vel.x, vel.y)
end

function client.update(dt)
   
end

function love.update(dt)
   if server.updateTimer < dt then
      server.update(server.updateTimeout)
      server.updateTimer = server.updateTimeout
   else
      server.updateTimer = server.updateTimer - dt
   end
   client.update(dt)
end

function love.draw()
   local p = server.getEstimatedPercent()
   local ship = client.ship
   local pos = Vect2i:lerp(ship.previous.pos, ship.current.pos, p)


   local ship_ang = lerp(ship.previous.ang, ship.current.ang, p)
   local ang = 6.28 - ship_ang
   love.graphics.draw(images.ship, pos.x, pos.y, ang, 1, 1, 24, 24)
end

